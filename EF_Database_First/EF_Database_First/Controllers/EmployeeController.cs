﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Transactions;
using System.Data.Entity;

namespace EF_Database_First.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeContext db = new EmployeeContext();

        public ActionResult Index()
        {
            return View(db.Employees.ToList());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            if (ModelState.IsValid)
            {
                /*db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");*/

                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Employees.Add(employee);
                        db.SaveChanges();

                        if (employee.Id > 0)
                        {
                            //int a = 0;
                            //int total = 10 / a;
                            Salary salary = new Salary();
                            salary.fk_id_employee = employee.Id;
                            salary.salary1 = employee.Salary;
                            db.Salaries.Add(salary);
                            db.SaveChanges();
                            ts.Commit();
                        }
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        ts.Rollback();
                        //---insert ex.message into database
                        ViewData["data"] = ex.Message;
                    }
                }
            }
            return View(employee);
        }
        public ActionResult Details(int id)
        {
            using (EmployeeContext dbModel = new EmployeeContext())
            {
                return View(dbModel.Employees.Where(x => x.Id == id).FirstOrDefault());
            }
        }

        public ActionResult Edit(int id)
        {
            Employee employee = db.Employees.Where(x => x.Id == id).FirstOrDefault();
            return View(employee);
        }

        [HttpPost]
        public ActionResult Edit(Employee employee)
        {
            /*db.Entry(employee).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();*/
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    db.Entry(employee).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    if(employee.Id > 0)
                    {
                        int a = 0;
                        int total = 10 / a;
                        Salary salary = db.Salaries.Where(x => x.fk_id_employee == employee.Id).FirstOrDefault();
                        salary.salary1 = employee.Salary;
                        salary.fk_id_employee = employee.Id;
                        db.Entry(salary).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                    }
                    return RedirectToAction("Index");
                }
                catch(Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            Employee employee = db.Employees.Where(x => x.Id == id).FirstOrDefault();
            return View(employee);
        }
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            using(DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    Salary salary = db.Salaries.Where(x => x.fk_id_employee == id).FirstOrDefault();
                    db.Salaries.Remove(salary);
                    db.SaveChanges();

                    Employee employee = db.Employees.Where(y => y.Id == id).FirstOrDefault();
                    db.Employees.Remove(employee);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    ViewData["data"] = ex.Message;
                }
            }
            return View();
        }
    }
}